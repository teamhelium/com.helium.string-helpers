<?php

namespace Helium\StringHelpers;

class StringHelpers
{
	/**
	 * @author Terrence Jackson
	 * @description Returns Random alpha numerical token of specified length.
	 * @param $len
	 * @return null|string
	 * @throws \Exception
	 */
	public static function randomAlphaNumericalToken ($len) {
		$length = ($len < 4) ? 4 : $len;
		return bin2hex(random_bytes(($length - ($length % 2)) / 2));
	}

	/**
	 * @author Terrence Jackson
	 * @description Returns Random numerical token of specified length.
	 * @param $len
	 * @return null|string|string[]
	 */
	public static function randomNumericalToken ($len) {

		$numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
		shuffle($numbers);
		$positions = array_rand($numbers, $len);

		$output = '';

		foreach ($positions as $position) {
			$output .= $numbers[$position];
		}

		return $output;
	}

	/**
	 * @author Terrence Jackson
	 * @description Returns Only Alpha Numeric Chars from String.
	 * @param $number
	 * @return null|string|string[]
	 */
	public static function onlyAlphaNumeric ($string) {
		return preg_replace('/[^a-zA-Z0-9]/', '', $string);
	}

	/**
	 * @author Terrence Jackson
	 * @description Returns Only Numeric Chars from String.
	 * @param $number
	 * @return null|string|string[]
	 */
	public static function onlyNumeric ($number) {
		return preg_replace('/[^0-9.]/', '', $number);
	}
}
